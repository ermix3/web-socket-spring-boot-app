# Define variables
DOCKER_COMPOSE := docker-compose
IMAGE_NAME := my-spring-boot-app
SPRING_PROFILE := default

# Build Docker image
build:
	docker build \
		-t $(IMAGE_NAME) \
		--build-arg SPRING_PROFILE=$(SPRING_PROFILE) \
		-f Dockerfile .

# Run Docker container
run:
	docker run \
		-e SPRING_PROFILES_ACTIVE=$(SPRING_PROFILE) \
		-p 8080:8080 \
		$(IMAGE_NAME)

clean-image:
	docker rmi -f $(IMAGE_NAME)

# Clean Docker environment
clean:
	docker stop $$(docker ps -a -q --filter="ancestor=$(IMAGE_NAME)")
	docker rm $$(docker ps -a -q --filter="ancestor=$(IMAGE_NAME)")
	docker rmi -f $(IMAGE_NAME)

# Docker Compose targets
compose-start:
	$(DOCKER_COMPOSE) up --build -d

compose-stop:
	$(DOCKER_COMPOSE) down

compose-clean:
	$(DOCKER_COMPOSE) down -v

compose-set-profile:
	$(DOCKER_COMPOSE) down
	@read -p "Enter Spring profile (default): " profile; \
	$(DOCKER_COMPOSE) up --build -d --env SPRING_PROFILES_ACTIVE=$${profile:-default}

# Help message
help:
	@echo "Usage: make [target] [VARIABLE=value]"
	@echo ""
	@echo "Dockerfile targets:"
	@echo "  build             Build Docker image"
	@echo "  run               Run Docker container"
	@echo "  clean-image       Clean Docker image"
	@echo "  clean       	   Clean Docker "
	@echo ""
	@echo "Docker Compose targets:"
	@echo "  compose-start     Build Docker images and start services"
	@echo "  compose-stop      Stop and remove Docker containers"
	@echo "  compose-clean     Clean Docker environment"
	@echo "  compose-set-profile Set active Spring profile and restart services"
	@echo ""
	@echo "General targets:"
	@echo "  help              Display this help message"
	@echo "VARIABLE:"
	@echo "  IMAGE_NAME        Name of the Docker image (default: my-spring-boot-app)"
	@echo "  SPRING_PROFILE    Active Spring profile (default: default)"


# Ensure targets are not treated as file targets
.PHONY: build run clean-image compose-start compose-stop compose-clean compose-set-profile help
