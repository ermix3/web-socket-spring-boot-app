LABEL authors="ermix"

FROM eclipse-temurin:17-jdk AS builder
RUN apt-get update && \
    apt-get install -y maven
WORKDIR /app
COPY . .
RUN mvn clean install

FROM eclipse-temurin:17-jdk
VOLUME /tmp
COPY target/*.jar ms-buynetna.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar","/ms-buynetna.jar"]
WORKDIR /app
COPY --from=builder /app/target/*.jar ms-buynetna.jar
ENTRYPOINT ["java", "-jar", "/app/ms-buynetna.jar"]

ENV SPRING_PROFILES_ACTIVE=default

EXPOSE 8080
