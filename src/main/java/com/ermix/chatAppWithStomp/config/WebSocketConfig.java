package com.ermix.chatAppWithStomp.config;

import com.ermix.chatAppWithStomp.entity.UserApp;
import com.ermix.chatAppWithStomp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Optional;

@Log
@Configuration
@RequiredArgsConstructor
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    private final UserRepository userRepository;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic", "/user", "/chatroom");
        config.setApplicationDestinationPrefixes("/app");
        config.setUserDestinationPrefix("/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").withSockJS();
    }

    @EventListener
    public void handleSessionEvent(SessionDisconnectEvent event) {
        updateUserStatus(event, false);
    }

    @EventListener
    public void handleSessionEvent(SessionConnectEvent event) {
        updateUserStatus(event, true);
    }

    private void updateUserStatus(AbstractSubProtocolEvent event, boolean online) {
        String username = getUsername(event);
        if (username != null) {
            Optional<UserApp> userOptional = Optional.ofNullable(userRepository.findByUsername(username));
            userOptional.ifPresent(user -> {
                user.setOnline(online);
                userRepository.save(user);
            });
            if (userOptional.isEmpty()) {
                log.warning("User not found");
            }
        } else {
            log.warning("Username is null");
        }
    }

    private String getUsername(AbstractSubProtocolEvent event) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        return accessor.getUser() != null ? accessor.getUser().getName() : null;
    }
}
