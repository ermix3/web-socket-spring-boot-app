package com.ermix.chatAppWithStomp.repository;

import com.ermix.chatAppWithStomp.entity.Task;
import com.ermix.chatAppWithStomp.entity.UserApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByUser(UserApp user);
}
