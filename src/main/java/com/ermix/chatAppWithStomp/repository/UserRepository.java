package com.ermix.chatAppWithStomp.repository;

import com.ermix.chatAppWithStomp.entity.UserApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<UserApp, Long> {
    UserApp findByUsername(String username);

    boolean existsByUsername(String username);

    List<UserApp> findByOnlineTrue();

    List<UserApp> findByOnlineFalse();
}