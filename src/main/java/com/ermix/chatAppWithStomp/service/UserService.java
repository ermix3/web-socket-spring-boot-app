package com.ermix.chatAppWithStomp.service;

import com.ermix.chatAppWithStomp.entity.UserApp;
import com.ermix.chatAppWithStomp.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public void setUserOnline(String username, boolean online) {
        UserApp user = userRepository.findByUsername(username);
        if (user != null) {
            user.setOnline(online);
            userRepository.save(user);
        }
    }


    public boolean isUserOnline(String recipient) {
        UserApp user = userRepository.findByUsername(recipient);
        return user != null && user.isOnline();
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public void saveUser(UserApp user) {
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserApp user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return user;
    }

    public UserApp getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<UserApp> getAllUsers() {
        return userRepository.findAll();
    }

    public List<UserApp> getOnlineUsers() {
        return userRepository.findByOnlineTrue();
    }

    public List<UserApp> getOfflineUsers() {
        return userRepository.findByOnlineFalse();
    }
}
