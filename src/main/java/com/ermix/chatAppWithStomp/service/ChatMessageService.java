package com.ermix.chatAppWithStomp.service;

import com.ermix.chatAppWithStomp.entity.ChatMessage;
import com.ermix.chatAppWithStomp.repository.ChatMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ChatMessageService {

    private final ChatMessageRepository chatMessageRepository;

    public List<ChatMessage> getPendingMessages(String recipient) {
        return chatMessageRepository.findByRecipientAndDeliveredIsFalse(recipient);
    }

    public void markMessagesAsDelivered(List<ChatMessage> messages) {
        messages.forEach(message -> {
            message.setDelivered(true);
            chatMessageRepository.save(message);
        });
    }
}