package com.ermix.chatAppWithStomp.service;

import com.ermix.chatAppWithStomp.request.TaskRequest;
import com.ermix.chatAppWithStomp.component.NotificationSender;
import com.ermix.chatAppWithStomp.entity.Notification;
import com.ermix.chatAppWithStomp.entity.Task;
import com.ermix.chatAppWithStomp.entity.UserApp;
import com.ermix.chatAppWithStomp.repository.TaskRepository;
import com.ermix.chatAppWithStomp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final NotificationSender notificationSender;
    private final UserRepository userRepository;

    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    public void createTask(TaskRequest taskRequest, UserApp currentUser) {
        UserApp assignedUser = userRepository.findByUsername(taskRequest.getUsername());

        Task newTask = Task.builder()
                .title(taskRequest.getTitle())
                .description(taskRequest.getDescription())
                .user(assignedUser)
                .build();

        taskRepository.save(newTask);

        if (!assignedUser.equals(currentUser)) {
            Notification notification = Notification.builder()
                    .sender(currentUser.getUsername())
                    .recipient(assignedUser.getUsername())
                    .message("You have been assigned a new task: " + taskRequest.getTitle())
                    .createdAt(LocalDateTime.now())
                    .build();
            notificationSender.send(notification);
        }

    }
}
