package com.ermix.chatAppWithStomp.controller;

import com.ermix.chatAppWithStomp.entity.Notification;
import com.ermix.chatAppWithStomp.repository.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationRepository notificationRepository;

    // This for send notification from the front-end, the URL: `/ws/app/notifications`
    @MessageMapping("/notifications")
    // This for broadcast the notification to all the users, the URL: `/ws/topic/notifications`
    @SendTo("/topic/notifications")
    public Notification sendNotification(Notification notification) {
        notificationRepository.save(notification);
        return notification;
    }

}
