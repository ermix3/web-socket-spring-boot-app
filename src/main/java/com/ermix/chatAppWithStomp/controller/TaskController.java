package com.ermix.chatAppWithStomp.controller;

import com.ermix.chatAppWithStomp.request.TaskRequest;
import com.ermix.chatAppWithStomp.entity.Task;
import com.ermix.chatAppWithStomp.entity.UserApp;
import com.ermix.chatAppWithStomp.service.TaskService;
import com.ermix.chatAppWithStomp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/tasks")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;
    private final UserService userService;

    @GetMapping
    public String getAllTasks(Model model) {
        List<Task> tasks = taskService.getAllTasks();
        model.addAttribute("tasks", tasks);
        return "tasks";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("taskRequest", new TaskRequest());
        model.addAttribute("users", userService.getAllUsers());
        return "task-create";
    }

    @PostMapping("/create")
    public String createTask(@ModelAttribute("taskRequest") TaskRequest taskRequest, Authentication authentication) {
        UserApp currentUser = userService.getUserByUsername(authentication.getName());
        taskService.createTask(taskRequest, currentUser);
        return "redirect:/tasks";
    }
}