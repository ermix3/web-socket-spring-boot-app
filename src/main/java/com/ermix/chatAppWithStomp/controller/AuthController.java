package com.ermix.chatAppWithStomp.controller;

import com.ermix.chatAppWithStomp.entity.UserApp;
import com.ermix.chatAppWithStomp.repository.NotificationRepository;
import com.ermix.chatAppWithStomp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;
    private final NotificationRepository notificationRepository;

    @GetMapping("/")
    public String showHome(Model model, Authentication authentication) {
        model.addAttribute("notifications", notificationRepository.findAllByRecipient(authentication.getName()));
        return "index";
    }

    @GetMapping("/signup")
    public String showSignUpForm(Model model) {
        model.addAttribute("user", new UserApp());
        return "signup";
    }

    @PostMapping("/signup")
    public String signUp(@ModelAttribute("user") UserApp user, BindingResult result) {
        if (result.hasErrors()) {
            return "signup";
        }

        if (userService.existsByUsername(user.getUsername())) {
            result.rejectValue("username", null, "Username is already taken");
            return "signup";
        }

        userService.saveUser(user);
        return "redirect:/signin";
    }

    @GetMapping("/signin")
    public String showLoginForm() {
        return "signin";
    }
}
