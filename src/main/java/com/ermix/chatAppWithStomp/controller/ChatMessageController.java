package com.ermix.chatAppWithStomp.controller;

import com.ermix.chatAppWithStomp.entity.ChatMessage;
import com.ermix.chatAppWithStomp.entity.Message;
import com.ermix.chatAppWithStomp.repository.ChatMessageRepository;
import com.ermix.chatAppWithStomp.service.ChatMessageService;
import com.ermix.chatAppWithStomp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ChatMessageController {
    private final ChatMessageRepository chatMessageRepository;

    private final ChatMessageService chatMessageService;
    private final UserService userService;

    private final SimpMessagingTemplate messagingTemplate;


    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.sendPrivateMessage")
    public void sendPrivateMessage(@Payload ChatMessage chatMessage) {
        if (userService.isUserOnline(chatMessage.getRecipient())) {
            messagingTemplate.convertAndSendToUser(
                    chatMessage.getRecipient(),
                    "/private",
                    chatMessage
            );
            chatMessage.setDelivered(true); // Mark message as delivered
        } else {
            chatMessage.setDelivered(false); // Message will be delivered when recipient is online
        }
        chatMessageRepository.save(chatMessage);
    }

    @MessageMapping("/connect")
    public void connect(Principal principal) {
        String username = principal.getName();
        List<ChatMessage> pendingMessages = chatMessageService.getPendingMessages(username);
        pendingMessages.forEach(message -> {
            messagingTemplate.convertAndSendToUser(
                    message.getRecipient(),
                    "/private",
                    message
            );
        });
        chatMessageService.markMessagesAsDelivered(pendingMessages);
    }


    // other methods
    @MessageMapping("/message")
    @SendTo("/chatroom/public")
    public Message receiveMessage(@Payload Message message) {
        return message;
    }

    @MessageMapping("/private-message")
    public Message recMessage(@Payload Message message) {
        messagingTemplate.convertAndSendToUser(message.getReceiverName(), "/private", message);
        System.out.println(message.toString());
        return message;
    }
}