package com.ermix.chatAppWithStomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatAppWithStompApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatAppWithStompApplication.class, args);
	}

}
