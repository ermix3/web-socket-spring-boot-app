package com.ermix.chatAppWithStomp.component;

import com.ermix.chatAppWithStomp.entity.Notification;
import com.ermix.chatAppWithStomp.repository.NotificationRepository;
import com.ermix.chatAppWithStomp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class NotificationSender {

    private final UserService userService;
    private final NotificationRepository notificationRepository;
    private final SimpMessagingTemplate messagingTemplate;

    public void send(Notification notification) {
        if (notification.getRecipient() != null) {
            if (userService.isUserOnline(notification.getRecipient())) {
                messagingTemplate.convertAndSendToUser(notification.getRecipient(), "/notifications", notification);
                notification.setReadAt(LocalDateTime.now());
            }
            notificationRepository.save(notification);
        } else {
            // users online
            messagingTemplate.convertAndSend("/topic/notifications", notification);
            notificationRepository.save(notification);

            // users offline
            userService.getOfflineUsers().forEach(user -> {
                if (!user.getUsername().equals(notification.getSender())) {
                    notification.setRecipient(user.getUsername());
                    notification.setReadAt(LocalDateTime.now());
                    notificationRepository.save(notification);
                }
            });
        }
    }
}